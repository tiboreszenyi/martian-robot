function Instruction(initialX, initialY, orientation, commands) {
    this.initialX = initialX;
    this.initialY = initialY;
    this.orientation = orientation;
    this.commands = commands;
}

module.exports = Instruction;