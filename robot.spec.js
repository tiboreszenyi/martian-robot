
var test = require("tape");
Robot = require("./robot.js");

test("Martian Robot test cases", function (testCases) {
    testCases.test("Position and orientation stored", function (assert) {
        var sut = new Robot(90, 1, 5);
        assert.equal(sut.orientation, 90);
        assert.equal(sut.positionX, 1);
        assert.equal(sut.positionY, 5);
        assert.end();
    });

    testCases.test("0 degrees is north", function (assert) {
        var sut = new Robot(0, 1, 1);
        assert.equal(sut.toString(), "1 1 N");
        assert.end();
    });

    testCases.test("90 degrees is east", function (assert) {
        var sut = new Robot(90, 1, 1);
        assert.equal(sut.toString(), "1 1 E");
        assert.end();
    });

    testCases.test("180 degrees is south", function (assert) {
        var sut = new Robot(180, 1, 1);
        assert.equal(sut.toString(), "1 1 S");
        assert.end();
    });

    testCases.test("270 degrees is west", function (assert) {
        var sut = new Robot(270, 1, 1);
        assert.equal(sut.toString(), "1 1 W");
        assert.end();
    });

    testCases.test("Simple move and rotate", function (assert) {
        var sut = new Robot(180, 1, 1, { x: 5, y: 3 }, []);
        sut.processCmd("FL");
        assert.equal(sut.toString(), "1 0 E");
        assert.end();
    });

    testCases.test("Going round - No position change", function (assert) {
        var sut = new Robot(90, 1, 1, { x: 5, y: 3 }, []);
        sut.processCmd("RFRFRFRF");
        assert.equal(sut.toString(), "1 1 E");
        assert.end();
    });

    testCases.test("Losing robot facing north", function (assert) {
        var sut = new Robot(0, 3, 2, { x: 5, y: 3 }, []);
        sut.processCmd("FRRFLLFFRRFLL");
        assert.equal(sut.toString(), "3 3 N LOST");
        assert.end();
    });

    testCases.test("Losing robot facing south", function (assert) {
        var sut = new Robot(90, 5, 2, { x: 50, y: 50 }, []);
        sut.processCmd("RFFF");
        assert.equal(sut.toString(), "5 0 S LOST");
        assert.end();
    });

    testCases.test("Losing robot facing east", function (assert) {
        var sut = new Robot(270, 45, 18, { x: 50, y: 50 }, []);
        sut.processCmd("LRRRFFFFFF");
        assert.equal(sut.toString(), "50 18 E LOST");
        assert.end();
    });

    testCases.test("Losing robot facing west", function (assert) {
        var sut = new Robot(180, 7, 42, { x: 50, y: 50 }, []);
        sut.processCmd("LLLFFFFFFFF");
        assert.equal(sut.toString(), "0 42 W LOST");
        assert.end();
    });

});