var fs = require('fs');
CommandParser = require("./commandParser.js");
Robot = require("./robot.js");

// read input from file
var input = fs.readFileSync("input.txt", "utf8");

// parse input string
var cmdParser;
try {
    cmdParser = new CommandParser(input);
}
catch (error) {
    console.log(`${error}`);
    process.exit();
}
var worldSize = cmdParser.getWorldSize();

// process all robot sequences
var scents = [];
var seq = cmdParser.getNextSequence();
while (seq !== null) {
    const robot = new Robot(seq.orientation, seq.initialX, seq.initialY, worldSize, scents);

    var scent = robot.processCmd(seq.commands);
    if (typeof scent !== "undefined") {
        scents.push(scent);
    }
    console.log(`${robot.toString()}`);

    seq = cmdParser.getNextSequence();
}
