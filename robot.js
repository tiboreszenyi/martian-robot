class Robot {

    constructor(orientation, positionX, positionY, worldSize, scents) {
        this.orientation = orientation;
        this.positionX = positionX;
        this.positionY = positionY;
        this.worldSize = worldSize;
        this.scents = scents;
        this.lost = false;
    }

    toDirection(orientation) {
        switch (orientation) {
            case 0: return "N";
            case 90: return "E";
            case 180: return "S";
            case 270: return "W";
        }
    }

    toString() {
        return `${this.positionX} ${this.positionY} ${this.toDirection(this.orientation)}${this.lost ? " LOST" : ""}`;
    }

    processCmd(cmd) {
        const length = cmd.length;
        for (var i = 0; i < length; i++) {
            switch (cmd[i]) {
                case "L":
                    this.turnLeft();
                    break;
                case "R":
                    this.turnRight();
                    break;
                case "F":
                    this.moveForward();
                    break;
            }

            if (this.lost)
                return { x: this.positionX, y: this.positionY, orientation: this.orientation };
        }
    }

    boundOrientation() {
        if (this.orientation < 0)
            this.orientation += 360;
        if (this.orientation >= 360)
            this.orientation -= 360;
    }

    compareScent(scent) {
        return this.positionX === scent.x && this.positionY === scent.y && this.orientation === scent.orientation;
    }

    checkScents() {
        return this.scents.some((scent) => {
            return this.compareScent(scent);
        });
    }

    turnLeft() {
        this.orientation -= 90;
        this.boundOrientation();
    }

    turnRight() {
        this.orientation += 90;
        this.boundOrientation();
    }

    moveForward() {
        if (this.checkScents())
            return;

        switch (this.orientation) {
            case 0:
                if (this.positionY + 1 > this.worldSize.y)
                    this.lost = true;
                else this.positionY++;
                break;
            case 90:
                if (this.positionX + 1 > this.worldSize.x)
                    this.lost = true;
                else this.positionX++;
                break;
            case 180:
                if (this.positionY - 1 < 0)
                    this.lost = true;
                else this.positionY--;
                break;
            case 270:
                if (this.positionX - 1 < 0)
                    this.lost = true;
                else this.positionX--;
                break;
        }
    }
}

module.exports = Robot;