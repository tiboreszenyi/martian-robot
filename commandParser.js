Instruction = require("./instruction.js");

class CommandParser {

    constructor(input) {
        const lines = input.split("\r\n");
        if (lines.length < 3) {
            throw Error("Invalid input.");
        }

        const coords = lines[0].split(/\s+/);
        this.worldSize = {
            x: parseInt(coords[0]),
            y: parseInt(coords[1])
        };
        if (isNaN(this.worldSize.x) || isNaN(this.worldSize.y))
            throw Error("Invalid world coordinates.");

        this.sequences = [];
        for (var i = 1; i < lines.length; i += 3) {
            const loc = lines[i].split(/\s+/);
            this.sequences.push(new Instruction(
                parseInt(loc[0]),
                parseInt(loc[1]),
                parseInt(this.toOrientation(loc[2])),
                lines[i + 1]));
        }

        this.currentSequence = -1;
    }

    toOrientation(direction) {
        switch (direction) {
            case "N": return 0;
            case "E": return 90;
            case "S": return 180;
            case "W": return 270;
        }
    }

    getWorldSize() {
        return this.worldSize;
    }

    getNextSequence() {
        this.currentSequence++;

        if (typeof this.sequences[this.currentSequence] === "undefined")
            return null;

        return this.sequences[this.currentSequence];
    }
}

module.exports = CommandParser;