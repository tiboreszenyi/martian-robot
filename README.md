# Martian Robot

This project implements the Martian Robots problem. The code is written for Node.js and was tested on Windows 10 with Node version 6.11.1 and npm version 3.10.10.

## Running the application
To run the application, type **npm start** or **node mars.js**.
Inputs are taken from the file input.txt. The input file uses the sample data from the problem description.

## Testing
For unit testing purposes, the project has a single dependency, Tape v4.70.

To install the dependency, type **npm i**.
To run the tests, type **npm test**.

*It is not necessary to install the dependency in order to run the application.*